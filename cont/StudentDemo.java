package bcas.edu.cont;

import java.util.Scanner;
import bcas.edu.demo.Student;

public class StudentDemo {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter The Name");	
		String name = scan.nextLine();
		
		System.out.println("Marks :");
		int marks = scan.nextInt();
		
		System.out.println("Address :");
		String address = scan.nextLine();
		
		
		Student S1 = new Student(name,marks,address);  
		Student S2 = new Student(name,marks,address);
		
		S1.getStudentsDetails();
		S2.getStudentsDetails();
	}
}
